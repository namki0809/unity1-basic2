﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameController : MonoBehaviour {

	public GameObject forList;
	public GameObject whileList;
	public GameObject oxList;

	int oxButton = 0;

	int gameList=0;


	public Text outPut;

	// Use this for initialization
	void Start () {

		
		
	}
	
	// Update is called once per frame
	void Update () {

		if(gameList==1)
		{

			outPut.text="달팽이는 이빨이 있다."; 
			
			if(oxButton==1)
			{

				outPut.text="정답입니다."; 
				gameList = 0;
				oxButton=0;
				

				 
			}
			else if (oxButton==2)
			{

				outPut.text="오답입니다."; 
				gameList=0;
				oxButton=0;
				
				
				
			}

		}

		if(gameList==2)
		{

			outPut.text="출력 버튼을 누르고,Console창을 확인해 주세요."; 
			gameList=0;

		}
		
		if(gameList==3)
		{

			outPut.text="출력 버튼을 누르고,Console창을 확인해 주세요."; 
			gameList=0;

		}
	
	}

	public void ForShowGugudan() {

          outPut.text="Console에 구구단을 출력했습니다."; 
          for (int a = 2; a < 10; a++)
            {
                for (int b = 1; b < 10; b++)
                {
					
					Debug.Log(a+"*"+b+"="+a*b); 

            }
        }
		
	}

	public void WhileShowGugudan() {

          int a = 2;
		  int b = 2;
          outPut.text="Console에 구구단을 출력했습니다."; 
          while(a <= 9)
		  {
			  b = 2;
			  while(b <= 9)
			  {

				  Debug.Log(a+"*"+b+"="+a*b); 
				  b++;

			  }
			  a++;
		  }
					
					

            
        
		
	}

	public void OXListOn(){
		
		gameList=1;
		oxList.SetActive(true);
		forList.SetActive(false);
		whileList.SetActive(false);			       

	}	

	public void ForGuguListOn(){
		
		oxList.SetActive(false);
		forList.SetActive(true);
		whileList.SetActive(false);
		gameList=2;

	}

	public void WhileGuguListOn(){
		
		oxList.SetActive(false);
		forList.SetActive(false);
		whileList.SetActive(true);
		gameList=3;

	}	

	public void ClickO(){

		oxButton=1;

	}

	public void ClickX(){

		oxButton=2;

	}

	

   
	
			

}

